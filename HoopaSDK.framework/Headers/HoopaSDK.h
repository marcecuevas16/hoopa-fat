//
//  HoopaSDK.h
//  HoopaSDK
//
//  Created by Carolina on 3/20/15.
//  Copyright (c) 2015 empatic. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HoopaSDK.
//FOUNDATION_EXPORT double CorePulsarVersionNumber;
FOUNDATION_EXPORT double HoopaSDKVersionNumber;

//! Project version string for HoopaSDK.
//FOUNDATION_EXPORT const unsigned char CorePulsarVersionString[];
FOUNDATION_EXPORT const unsigned char HoopaSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HoopaSDK/PublicHeader.h>


